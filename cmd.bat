@echo off
REM Remove Oracles's Java
set path=%path:C:\ProgramData\Oracle\Java\javapath;=%
REM Remove installed Nodejs
set path=%path:C:\Program Files\nodejs\;=%
REM Remove installed VS Code
set path=%path:C:\Program Files (x86)\Microsoft VS Code\bin;=%

REM Add the rest below
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_131
set SBT_HOME=C:\Users\kjayme\tools\sbt
REM set SBT_HOME=C:\Users\kjayme\tools\sbt-1.3.3
set M2_HOME=C:\Users\kjayme\tools\apache-maven-3.3.9
set NODEJS_HOME=C:\Users\kjayme\tools\node-v8.12.0-win-x64
set VS_CODE_HOME=C:\Users\kjayme\tools\VSCode-win32-x64-1.28.2
set ANT_HOME=C:\Users\kjayme\tools\apache-ant-1.9.9
REM set PYTHON_HOME=C:\Python27
set path=%path%;%JAVA_HOME%\bin;%SBT_HOME%\bin;%M2_HOME%\bin;%NODEJS_HOME%\;%VS_CODE_HOME%\;%ANT_HOME%\bin;

cd %USERPROFILE%
%windir%\system32\cmd.exe
