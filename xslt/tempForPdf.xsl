<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master margin-right="1cm" margin-left="1cm" margin-bottom="1cm" margin-top="1cm" page-width="20cm" page-height="30cm" master-name="default-page">
                    <fo:region-body/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="default-page" format="1">
                <fo:flow flow-name="xsl-region-body">
                    <fo:block-container font-size="10px" color="#58595B">
                        <fo:table border="1">
                            <fo:table-body>
                                <fo:table-row background-color="#9acd32">
                                    <fo:table-cell >
                                        <fo:block>Title</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block>Artist</fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                <xsl:for-each select="catalog/cd">
                                    <fo:table-row>
                                        <fo:table-cell >
                                            <fo:block><xsl:value-of select="title"/></fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell>
                                            <fo:block><xsl:value-of select="artist"/></fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </xsl:for-each>
                            </fo:table-body>
                        </fo:table>
                    </fo:block-container>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

</xsl:stylesheet> 